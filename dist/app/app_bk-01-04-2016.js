/* INTOURIST AUTOCOMPLETE
 -------------------------------------------------- */

var IntouristApp = angular.module('IntouristApp', ['IntouristControllers']);
var IntouristControllers = angular.module('IntouristControllers', []);

IntouristControllers.controller('IntouristMainCtrl', function ($scope, $http, $element) {

    $scope.inputCountry = $('input[name="selected-country"]').val();
    $scope.inputRegion = $('input[name="selected-region"]').val();
    $scope.inputHotel = $('input[name="selected-hotel"]').val();
    $scope.inputCity = $('input[name="selected-city"]').val();

    $scope.autocompleteWhere = {
        value: $('input[name="destination"]').val()
    };

    $scope.autocompleteCity = {
        value: $('input[name="StartPointName"]').val()
    };

    $scope.autocompleteHotel = {
        value: $('input[name="destination-hotel"]').val()
    };


    if (window.RuInturistStore && window.RuInturistStore.destination && window.RuInturistStore.destination.COUNTRY) {
        $scope.country = window.RuInturistStore.destination.COUNTRY;
    } else {

        $http.post("/local/ajax/destination.php?view[]=COUNTRY").then(function (res) {

            if (res)
                $scope.country = res.data.COUNTRY;
            else
                $scope.country = [];
        });


    }


    /*!
     * id: id региона
     * parent: id страны к коророй относится регион
     * title: наименование регоина
     * disabled: вкл/выкл регоин
     */

    if (window.RuInturistStore && window.RuInturistStore.destination && window.RuInturistStore.destination.REGION) {
        $scope.region = window.RuInturistStore.destination.REGION;
    } else {
        $http.post("/local/ajax/destination.php?view[]=REGION").then(function (res) {

            if (res)
                $scope.region = res.data.REGION;
            else
                $scope.region = [];
        });
    }


    /*!
     * id: id отеля
     * parent: id региона к коророму относится отель
     * title: наименование отеля
     * disabled: вкл/выкл отель
     */

    if (window.RuInturistStore && window.RuInturistStore.destination && window.RuInturistStore.destination.HOTEL) {
        $scope.hotel = window.RuInturistStore.destination.HOTEL;
    } else {
        $http.post("/local/ajax/destination.php?view[]=HOTEL").then(function (res) {

            if (res)
                $scope.hotel = res.data.HOTEL;
            else
                $scope.hotel = [];
        });
    }

    /*!
     * id: id города
     * title: наименование регоина
     * disabled: вкл/выкл регоин
     */

    if (window.RuInturistStore && window.RuInturistStore.startPoint) {
        $scope.city = window.RuInturistStore.startPoint;
    } else {
        $scope.city = [];
    }


    $scope.setCountry = function (data) {
        $scope.country = data;
    };

    $scope.setRegion = function (data) {
        $scope.region = data;
    };

    $scope.setHotel = function (data) {
        $scope.hotel = data;
    };

    $scope.setCity = function (data) {
        $scope.city = data;
    };

    $scope.setInputCountry = function (data) {
        $scope.inputCountry = data;
    };

    $scope.setInputRegion = function (data) {
        $scope.inputRegion = data;
    };

    $scope.setInputHotel = function (data) {
        $scope.inputHotel = data;
    };

    $scope.setInputCity = function (data) {
        $scope.inputCity = data;
    };

    $scope.setAutocompleteWhere = function (data) {
        $scope.autocompleteWhere.value = data;
    };

    $scope.setAutocompleteCity = function (data) {
        $scope.autocompleteCity.value = data;
    };

    $scope.setAutocompleteHotel = function (data) {
        $scope.autocompleteHotel.value = data;
    };

    $scope.countUniqueLength = function (arr) {
        if (arr.length == 0) return 0;

        var unique_cnt = 1;
        var unique_flag = true;

        for (var i = 1; i < arr.length; i++) {
            unique_flag = true;

            for (var j = i - 1; j >= 0; j--) {
                if (arr[j] == arr[i]) unique_flag = false;
                if (unique_flag) unique_cnt++;
            }
            ;
        }
        ;

        return unique_cnt;
    };

    $scope.autocompleteSelectCountryByRegion = function (parent) {
        if($scope.country){
            var filteredCountry = $scope.country.filter(function (item) {
                return item.id === parent;
            });

            return filteredCountry[0].title;
        }

    };

    $scope.autocompleteSelectCountryByHotel = function (parent) {
        if($scope.region){
            var filteredRegion = $scope.region.filter(function (item) {
                return item.id === parent;
            });
        }

        if($scope.country){
            var filteredCountry = $scope.country.filter(function (item) {
                return item.id === filteredRegion[0].parent;
            });


            return filteredCountry[0].title;
        }
    };


    $('#search-form-reset').on('click', function () {
        $scope.setAutocompleteWhere('');
        $scope.setAutocompleteCity('');
    });

});

IntouristControllers.controller('AutocompleteWhereCtrl', function ($scope, $http, $element, $timeout) {


    $scope.selectedCountry = '';
    $scope.selectedRegion = '';

    $scope.uniqueHotels = [];
    $scope.uniqueRegions = [];

    $scope.inFocus = false;

    $scope.autocompleteToggle = function ($event) {
        if ($event.type == 'focus') {
            $timeout(function () {
                $scope.inFocus = true;
            }, 100);
            $scope.autocompleteVisibility = true;
        }
        ;

        if ($event.type == 'blur') {
            $scope.inFocus = false;
            $scope.autocompleteVisibility = false;
        }
        ;

        if ($event.type == 'click' && $scope.inFocus) {
            if ($scope.autocompleteWhere.value == '') {
                $scope.autocompleteVisibility = !$scope.autocompleteVisibility;
            }
            ;
        }
        ;
    };

    $scope.selectCountryAndRegion = function (value) {
        $scope.selectedCountry = '';
        $scope.selectedRegion = '';
        $scope.uniqueHotels.length = 0;
        $scope.uniqueRegions.length = 0;
        $scope.setAutocompleteHotel('');


        if (typeof $scope.filteredHotels != 'undefined') {
            if ($scope.filteredHotels.length) {
                for (var i = 0; i < $scope.filteredHotels.length; i++) {
                    $scope.uniqueHotels.push($scope.filteredHotels[i].parent);
                }
                ;

                if ($scope.countUniqueLength($scope.uniqueHotels) == 1) {
                    var filteredRegion = $scope.region.filter(function (item) {
                        return item.id === $scope.filteredHotels[0].parent;
                    });

                    if($scope.country){
                        var filteredCountry = $scope.country.filter(function (item) {
                            return item.id === filteredRegion[0].parent;
                        });
                    }

                    if(filteredCountry){
                        $scope.selectedCountry = filteredCountry[0].title;
                    }

                    $scope.selectedRegion = filteredRegion[0].title;
                    $scope.selectedHotel = $scope.filteredHotels[0].title;
                }
                ;
            }
            ;
        }
        ;

        if (typeof $scope.filteredRegions != 'undefined') {
            if ($scope.filteredRegions.length && $scope.filteredRegions.length == 1) {
                if($scope.country){
                    var filteredCountry = $scope.country.filter(function (item) {
                        return item.id === $scope.filteredRegions[0].parent;
                    });

                    $scope.selectedCountry = filteredCountry[0].title;
                }
            }
            ;
        }
        ;
    };

    $scope.autocompleteSetValue = function (value) {
        /*$scope.autocompleteWhere = value;*/
        $scope.setAutocompleteWhere(value);
        $scope.autocompleteVisibility = true;

        $scope.$watch('autocompleteWhere', function (newValue, oldValue) {
            $scope.selectCountryAndRegion(newValue);
        });

    };

    // autocomplete select values


    // Inputs

    $scope.selectForInputCountry = function (obj) {
        $scope.setInputCountry(obj.id);
        $scope.setInputRegion('');
        $scope.setInputHotel('');
    };

    $scope.selectForInputRegion = function (obj) {
        var objectCountry = $scope.country.filter(function (item) {
            return item.id === obj.parent;
        });

        $scope.setInputCountry(objectCountry[0].id);
        $scope.setInputRegion(obj.id);
        $scope.setInputHotel('');
    };

    $scope.selectForInputHotel = function (obj) {
        var objectRegion = $scope.region.filter(function (item) {
            return item.id === obj.parent;
        });

        var objectCountry = $scope.country.filter(function (item) {
            return item.id === objectRegion[0].parent;
        });

        $scope.setInputCountry(objectCountry[0].id);
        $scope.setInputRegion(objectRegion[0].id);
        $scope.setInputHotel(obj.id);
    };

    $scope.autocompleteClear = function () {
        $scope.setInputCountry('');
        $scope.setInputRegion('');
        $scope.setInputHotel('');
        $scope.setAutocompleteWhere('');
        $scope.setAutocompleteHotel('');
    };

    $scope.searchFilter = function (item) {
        return (
        angular.lowercase(item.value).indexOf(angular.lowercase($scope.autocompleteWhere.value) || '') !== -1 ||
        angular.lowercase(item.value_wr).indexOf(angular.lowercase($scope.autocompleteWhere.value) || '') !== -1 ||
        angular.lowercase(item.value_en).indexOf(angular.lowercase($scope.autocompleteWhere.value) || '') !== -1
        );
    };

});

IntouristControllers.controller('AutocompleteCityCtrl', function ($scope, $http, $element, $timeout) {
    /*$scope.inputCity = '';*/
    $scope.inFocus = false;

    $scope.autocompleteToggle = function ($event) {
        if ($event.type == 'focus') {
            $timeout(function () {
                $scope.inFocus = true;
            }, 100);
            $scope.autocompleteVisibility = true;
        }
        ;

        if ($event.type == 'blur') {
            $scope.inFocus = false;
            $scope.autocompleteVisibility = false;
        }
        ;

        if ($event.type == 'click' && $scope.inFocus) {
            if ($scope.autocompleteCity.value == '') {
                $scope.autocompleteVisibility = !$scope.autocompleteVisibility;
            }
            ;
        }
        ;
    };

    $scope.autocompleteSetValue = function (value) {
        $scope.setAutocompleteCity(value);
    };

    $scope.autocompleteClear = function () {
        $scope.setInputCity('');
        $scope.setAutocompleteCity('');
    };

    // Inputs

    $scope.selectForInputRegion = function (obj) {
        $scope.setInputCity(obj.id);
    };

    $scope.searchFilter = function (item) {
        return (
        angular.lowercase(item.value).indexOf(angular.lowercase($scope.autocompleteCity.value) || '') !== -1 ||
        angular.lowercase(item.value_wr).indexOf(angular.lowercase($scope.autocompleteCity.value) || '') !== -1 ||
        angular.lowercase(item.value_en).indexOf(angular.lowercase($scope.autocompleteCity.value) || '') !== -1
        );
    };

});

IntouristControllers.controller('AutocompleteHotelCtrl', function ($scope, $http, $element, $timeout) {
    /*$scope.inputHotel = '';*/
    $scope.inFocus = false;

    /*
    $scope.autocompleteToggle = function ($event) {

        if ($event.type == 'focus') {
            
            $timeout(function () {
                $scope.inFocus = true;
            }, 100);
            $scope.autocompleteVisibility = true;
        }
        ;

        if ($event.type == 'blur') {
            $scope.inFocus = false;
            $scope.autocompleteVisibility = false;
        }
        ;

        if ($event.type == 'click' && $scope.inFocus) {
            if ($scope.autocompleteHotel.value == '') {
                $scope.autocompleteVisibility = !$scope.autocompleteVisibility;
            }
            ;
        }
        ;
    };
    */


/*
    $scope.autocompleteToggle = function (toggle) {
        $scope.autocompleteVisibility = toggle;
    };
*/

    $scope.autocompleteToggle = function ($event) {
        if ($event.type == 'focus') {
            $timeout(function () {
                $scope.inFocus = true;
            }, 100);
            $scope.autocompleteVisibility = true;
        };

        if ($event.type == 'blur') {
            $scope.inFocus = false;
            $scope.autocompleteVisibility = false;
        };

        if ($event.type == 'click' && $scope.inFocus) {
            if ($scope.autocompleteHotel.value == '') {
                $scope.autocompleteVisibility = !$scope.autocompleteVisibility;
            };
        };
    };

    $scope.autocompleteSetValue = function (value) {
        $scope.setAutocompleteHotel(value);
    };

    $scope.autocompleteClear = function () {
        /*$scope.inputHotel = '';*/
        $scope.setInputHotel('');
        $scope.setInputRegion('');
        $scope.setInputCountry('');
        /*$scope.autocompleteHotel = '';*/
        $scope.setAutocompleteHotel('');
        $scope.setAutocompleteWhere('');
    };

    // Inputs

    /*  $scope.selectForInputHotel = function(obj) {
     $scope.inputHotel = '{hotelId: '+ obj.id +'}';
     };*/

    $scope.selectForInputHotel = function (obj) {
        var objectRegion = $scope.region.filter(function (item) {
            return item.id === obj.parent;
        });

        var objectCountry = $scope.country.filter(function (item) {
            return item.id === objectRegion[0].parent;
        });

        $scope.setInputCountry(objectCountry[0].id);
        $scope.setInputRegion(objectRegion[0].id);
        $scope.setInputHotel(obj.id);
        $scope.setAutocompleteWhere(objectRegion[0].title);
    };

    $scope.searchFilter = function (item) {
        return (angular.lowercase(item.title).indexOf(angular.lowercase($scope.autocompleteHotel.title) || '') !== -1);
        //return (angular.lowercase(item.value).indexOf(angular.lowercase($scope.autocompleteHotel.value) || '') !== -1);
    };
/*
    $scope.searchFilter = function (item) {
        return (
        angular.lowercase(item.value).indexOf(angular.lowercase($scope.autocompleteHotel.value) || '') !== -1 ||
        angular.lowercase(item.value_wr).indexOf(angular.lowercase($scope.autocompleteHotel.value) || '') !== -1 ||
        angular.lowercase(item.value_en).indexOf(angular.lowercase($scope.autocompleteHotel.value) || '') !== -1
        );
    };
*/
});