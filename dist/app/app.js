/* INTOURIST AUTOCOMPLETE
 -------------------------------------------------- */

var IntouristApp = angular.module('IntouristApp', ['IntouristControllers']);

var IntouristControllers = angular.module('IntouristControllers', []);

IntouristControllers.controller('IntouristMainCtrl', function ($scope, $http, $element) {

    $scope.inputCountry = $('input[name="selected-country"]').val();
    $scope.inputRegion = $('input[name="selected-region"]').val();
    $scope.inputHotel = $('input[name="selected-hotel"]').val();
    $scope.inputCity = $('input[name="selected-city"]').val();

    $scope.autocompleteWhere = {
        value: $('input[name="destination"]').val()
    };

    $scope.autocompleteCity = {
        value: $('input[name="StartPointName"]').val()
    };

    $scope.autocompleteHotel = {
        value: $('input[name="destination-hotel"]').val()
    };

    var lsCountry = getFromLocalStorage("lsCountry");

    if(lsCountry){
        $scope.country = JSON.parse(lsCountry);
    } else {
        $http.post("/local/ajax/destination.php?view[]=COUNTRY").then(function (res) {
            if (res)
                $scope.country = res.data.COUNTRY;
            else
                $scope.country = [];

            setToLocalStorage("lsCountry", JSON.stringify($scope.country));
        });

    }


    /*!
     * id: id региона
     * parent: id страны к коророй относится регион
     * title: наименование регоина
     * disabled: вкл/выкл регоин
     */
    var lsRegion = getFromLocalStorage("lsRegion");
    if(lsRegion){
        $scope.region = JSON.parse(lsRegion);
        $('input[name=destination]').removeClass('input__loading');
    } else {
        $http.post("/local/ajax/destination.php?view[]=REGION").then(function (res) {

            if (res)
                $scope.region = res.data.REGION;
            else
                $scope.region = [];

            setToLocalStorage("lsRegion", JSON.stringify($scope.region));

            $('input[name=destination]').removeClass('input__loading');
        });
    }


    /*!
     * id: id отеля
     * parent: id региона к коророму относится отель
     * title: наименование отеля
     * disabled: вкл/выкл отель
     */
    /*
    var lsHotel = getFromLocalStorage("lsHotel");
    if(lsHotel){

        $scope.hotel = JSON.parse(lsHotel);
        $('input[name=destination]').removeClass('input__loading');
        $('input[name=destination-hotel]').removeClass('input__loading');
        console.log('lsHotel 1!!!');

    } else {
        $http.post("/local/ajax/destination.php?view[]=HOTEL").then(function (res) {

            if (res)
                $scope.hotel = res.data.HOTEL;
            else
                $scope.hotel = [];

            setToLocalStorage("lsHotel", JSON.stringify(res.data.HOTEL));

            $('input[name=destination]').removeClass('input__loading');
            $('input[name=destination-hotel]').removeClass('input__loading');
            console.log('lsHotel 22!!!');
        });
    }
    */

    $scope.hotel = [];

    /*!
     * id: id города
     * title: наименование регоина
     * disabled: вкл/выкл регоин
     */

    if (window.RuInturistStore && window.RuInturistStore.startPoint) {
        $scope.city = window.RuInturistStore.startPoint;
        $('input[name=StartPointName]').removeClass('input__loading');
    } else {
        $scope.city = [];
    }


    $scope.setCountry = function (data) {
        $scope.country = data;
    };

    $scope.setRegion = function (data) {
        $scope.region = data;
    };

    $scope.setHotel = function (data) {
        $scope.hotel = data;
    };

    $scope.setCity = function (data) {
        $scope.city = data;
    };

    $scope.setInputCountry = function (data) {
        $scope.inputCountry = data;
        refreshAvaliableDates({inputCountry: data, inputCity: $scope.inputCity});
    };

    $scope.setInputRegion = function (data) {
        $scope.inputRegion = data;
    };

    $scope.setInputHotel = function (data) {
        $scope.inputHotel = data;
    };

    $scope.setInputCity = function (data) {
        $scope.inputCity = data;
        refreshAvaliableDates({inputCity: data, inputCountry: $scope.inputCountry});
    };

    $scope.setAutocompleteWhere = function (data) {
        resetErrorField('input[name=destination]', '.input-from');
        $scope.autocompleteWhere.value = data;
    };

    $scope.setAutocompleteCity = function (data) {
        resetErrorField('input[name=StartPointName]', '.input-to');
        $scope.autocompleteCity.value = data;
    };

    $scope.setAutocompleteHotel = function (data) {
        $scope.autocompleteHotel.value = data;
    };

    $('#search-form-reset').on('click', function () {
        $scope.setAutocompleteWhere('');
        $scope.setAutocompleteCity('');
    });

});

IntouristControllers.controller('AutocompleteWhereCtrl', function ($scope, $http, $element, $timeout) {
    $scope.selectedCountry = '';
    $scope.selectedCountryId = '';
    $scope.selectedRegion = '';
    $scope.selectedRegionId = '';
    $scope.selectedRegionCountry = '';
    $scope.selectedHotel = '';
    $scope.selectedHotelId = '';
    $scope.selectedHotelCountry = '';
    $scope.filteredCountriesArr = [];
    $scope.filteredRegionsArr = [];
    $scope.filteredHotelsArr = [];

    $scope.uniqueHotels = [];
    $scope.uniqueRegions = [];
    $scope.inFocus = false;
    $scope.popularRegions = [];
    $scope.popularHotels = [];

    var getHotelsList = function () {

        if($scope.autocompleteWhere.value){
            jQuery.ajax({
                method: 'POST',
                url: '/local/include/ajax/get-hotels-list.php',
                dataType: 'json',
                data: {
                    NAME: $scope.autocompleteWhere.value
                },
            }).done(function (data) {
                $scope.$apply(function () {
                    if (data.length) {
                        $scope.setHotel(jQuery.parseJSON(data));
                    } else {
                        $scope.setHotel([]);
                    }
                });
                /*
                console.log('ajax request for hotels done');
                if (data.length) {
                    console.log(jQuery.parseJSON(data));
                }
                ;
                */
            });
        }else{
            $scope.setHotel([]);
        }
    };

    var delayed = _.throttle(getHotelsList, 500);

    $scope.$watch('autocompleteWhere.value', function() {
      delayed();
    });

    $scope.autocompleteToggle = function ($event) {
        if ($event.type == 'focus') {
            $timeout(function () {
                $scope.inFocus = true;
            }, 100);
            $scope.autocompleteVisibility = true;
        }

        if ($event.type == 'blur') {
            $scope.inFocus = false;
            $scope.autocompleteVisibility = false;
        }

        if ($event.type == 'click' && $scope.inFocus) {
            if ($scope.autocompleteWhere.value == '') {
                $scope.autocompleteVisibility = !$scope.autocompleteVisibility;
            }
        }

        if ($event.type == 'keyup') {
            $scope.inFocus = false;
            $scope.autocompleteVisibility = true;

            $timeout(function () {
                var filteredCountry = $scope.country.filter(function (item) {
                    return (
                    (item.value && angular.lowercase(item.value) === angular.lowercase($scope.autocompleteWhere.value)) ||
                    (item.value_en && angular.lowercase(item.value_en) === angular.lowercase($scope.autocompleteWhere.value)) ||
                    (item.value_wr && angular.lowercase(item.value_wr) === angular.lowercase($scope.autocompleteWhere.value))
                    )
                });

                var filteredRegion = $scope.region.filter(function (item) {
                    return (
                    (item.value && angular.lowercase(item.value) === angular.lowercase($scope.autocompleteWhere.value)) ||
                    (item.value_en && angular.lowercase(item.value_en) === angular.lowercase($scope.autocompleteWhere.value)) ||
                    (item.value_wr && angular.lowercase(item.value_wr) === angular.lowercase($scope.autocompleteWhere.value))
                    )
                });

                var filteredHotel = $scope.hotel.filter(function (item) {
                    return (
                    angular.lowercase(item.value) === angular.lowercase($scope.autocompleteWhere.value)
                    )
                });

                if (filteredCountry.length == 1) {
                    $scope.setInputCountry(filteredCountry[0].id);
                    $scope.autocompleteSetCountryValue(filteredCountry[0].value, filteredCountry[0].id);

                } else if (filteredRegion.length == 1) {
                    $scope.setInputCountry(filteredRegion[0].parent);
                    $scope.setInputRegion(filteredRegion[0].id);
                    $scope.autocompleteSetRegionValue(filteredRegion[0].value, filteredRegion[0].id, filteredRegion[0].parent);

                } else if (filteredHotel.length == 1) {
                    $scope.setInputCountry(filteredHotel[0].countryId);
                    $scope.setInputRegion(filteredHotel[0].parent);
                    $scope.setInputHotel(filteredHotel[0].id);
                    $scope.autocompleteSetHotelValue(filteredHotel[0].value, filteredHotel[0].id, filteredHotel[0].parent, filteredHotel[0].countryId);

                } else {
                    $scope.setInputCountry('');
                    $scope.setInputRegion('');
                    $scope.setInputHotel('');

                    $scope.selectedCountry = '';
                    $scope.selectedCountryId = '';
                    $scope.selectedRegion = '';
                    $scope.selectedRegionId = '';
                    $scope.selectedRegionCountry = '';
                    $scope.selectedHotel = '';
                    $scope.selectedHotelId = '';
                    $scope.selectedHotelCountry = '';
                    $scope.filteredCountriesArr = [];
                    $scope.filteredRegionsArr = [];
                    $scope.filteredHotelsArr = [];
                }
                ;
            }, 100);
        }
    };

    $scope.autocompleteSetCountryValue = function (value, id) {
        $scope.selectedCountry = '';
        $scope.selectedCountryId = '';
        $scope.selectedRegion = '';
        $scope.selectedRegionId = '';
        $scope.selectedRegionCountry = '';
        $scope.selectedHotel = '';
        $scope.selectedHotelId = '';
        $scope.selectedHotelCountry = '';
        $scope.filteredCountriesArr = [];
        $scope.filteredRegionsArr = [];
        $scope.filteredHotelsArr = [];

        $scope.setAutocompleteHotel('');

        $scope.setAutocompleteWhere(value);
        $scope.autocompleteVisibility = true;

        $scope.$watch('autocompleteWhere', function (newValue, oldValue) {
            //country
            $scope.filteredCountriesArr = $scope.country.filter(function (item) {
                return item.id == id;
            });

            $scope.selectedCountry = $scope.filteredCountriesArr[0].value;
            $scope.selectedCountryId = $scope.filteredCountriesArr[0].id;
        });
    };

    $scope.autocompleteSetRegionValue = function (value, id, parent) {
        $scope.selectedCountry = '';
        $scope.selectedCountryId = '';
        $scope.selectedRegion = '';
        $scope.selectedRegionId = '';
        $scope.selectedRegionCountry = '';
        $scope.selectedHotel = '';
        $scope.selectedHotelId = '';
        $scope.selectedHotelCountry = '';
        $scope.filteredCountriesArr = [];
        $scope.filteredRegionsArr = [];
        $scope.filteredHotelsArr = [];

        $scope.setAutocompleteHotel('');

        $scope.setAutocompleteWhere(value);
        $scope.autocompleteVisibility = true;

        $scope.$watch('autocompleteWhere', function (newValue, oldValue) {
            //country
            $scope.filteredCountriesArr = $scope.country.filter(function (item) {
                return item.id == parent;
            });

            $scope.selectedCountry = $scope.filteredCountriesArr[0].value;
            $scope.selectedCountryId = $scope.filteredCountriesArr[0].id;

            //region
            $scope.filteredRegionsArr = $scope.region.filter(function (item) {
                return item.id == id;
            });

            $scope.selectedRegion = $scope.filteredRegionsArr[0].value;
            $scope.selectedRegionId = $scope.filteredRegionsArr[0].id;
            $scope.selectedRegionParent = $scope.filteredRegionsArr[0].parent;
        });
    };

    $scope.autocompleteSetHotelValue = function (value, id, parent, countryId) {
        $scope.selectedCountry = '';
        $scope.selectedCountryId = '';
        $scope.selectedRegion = '';
        $scope.selectedRegionId = '';
        $scope.selectedRegionCountry = '';
        $scope.selectedHotel = '';
        $scope.selectedHotelId = '';
        $scope.selectedHotelCountry = '';
        $scope.filteredCountriesArr = [];
        $scope.filteredRegionsArr = [];
        $scope.filteredHotelsArr = [];

        $scope.setAutocompleteWhere(value);
        $scope.autocompleteVisibility = true;

        $scope.$watch('autocompleteWhere', function (newValue, oldValue) {
            //country
            $scope.filteredCountriesArr = $scope.country.filter(function (item) {
                return item.id == countryId;
            });

            $scope.selectedCountry = $scope.filteredCountriesArr[0].value;
            $scope.selectedCountryId = $scope.filteredCountriesArr[0].id;

            //region
            $scope.filteredRegionsArr = $scope.region.filter(function (item) {
                return item.id == parent;
            });

            $scope.selectedRegion = $scope.filteredRegionsArr[0].value;
            $scope.selectedRegionId = $scope.filteredRegionsArr[0].id;
            $scope.selectedRegionParent = $scope.filteredRegionsArr[0].parent;

            //Hotel
            $scope.filteredHotelsArr = $scope.hotel.filter(function (item) {
                return item.id == id;
            });

            $scope.selectedHotel = $scope.filteredHotelsArr[0].value;
            $scope.selectedHotelId = $scope.filteredHotelsArr[0].id;
            $scope.selectedHotelCountry = $scope.filteredHotelsArr[0].countryId;
        });
    };

    // Inputs

    $scope.autocompleteSelectCountry = function (parent) {
        var filteredCountry = $scope.country.filter(function (item) {
            return item.id === parent;
        });

        return filteredCountry[0].value;
    };

    $scope.autocompleteSelectRegion = function (parent, parent2) {
      var filteredRegion = $scope.region.filter(function (item) {
        if (parent) {
          return item.id === parent;
        } else {
          return item.id === parent2;
        }
      });

      if (filteredRegion.length > 0) {
        return filteredRegion[0].value;
      }
    };

    $scope.selectForInputCountry = function (obj) {
        $scope.setInputCountry(obj.id);
        $scope.setInputRegion('');
        $scope.setInputHotel('');
    };

    $scope.selectForInputRegion = function (obj) {
        $scope.setInputCountry(obj.parent);
        $scope.setInputRegion(obj.id);
        $scope.setInputHotel('');
    };

    $scope.selectForInputHotel = function (obj) {
        $scope.setInputCountry(obj.countryId);
        $scope.setInputRegion(obj.parent);
        $scope.setInputHotel(obj.id);
    };

    $scope.autocompleteClear = function () {
        $scope.setInputCountry('');
        $scope.setInputRegion('');
        $scope.setInputHotel('');
        $scope.setAutocompleteWhere('');
        $scope.setAutocompleteHotel('');
        $scope.popularHotels = [];
        $scope.popularRegions = [];
        $scope.selectedCountry = '';
        $scope.selectedCountryId = '';
        $scope.selectedRegion = '';
        $scope.selectedRegionId = '';
        $scope.selectedRegionCountry = '';
        $scope.filteredCountriesArr = [];
        $scope.filteredRegionsArr = [];
        $scope.filteredHotelsArr = [];
    };

    $scope.searchPopularRegionsFilter = function (item) {
        if (typeof $scope.filteredCountriesArr !== 'undefined' && $scope.filteredCountriesArr.length == 1 && $scope.filteredRegionsArr.length != 1) {
            if (item.parent == $scope.filteredCountriesArr[0].id) {
                return true;
            }
            ;
        } else if (typeof $scope.filteredRegionsArr !== 'undefined' && $scope.filteredRegionsArr.length == 1 && $scope.filteredCountriesArr.length == 1) {
            if (item.id == $scope.selectedRegionId) {
                return true;
            }
        } else {
            return (
            (item.value && angular.lowercase(item.value).indexOf(angular.lowercase($scope.autocompleteWhere.value) || '') !== -1) ||
            (item.value_en && angular.lowercase(item.value_en).indexOf(angular.lowercase($scope.autocompleteWhere.value) || '') !== -1) ||
            (item.value_wr && angular.lowercase(item.value_wr).indexOf(angular.lowercase($scope.autocompleteWhere.value) || '') !== -1)
            );
        }
    };

    $scope.searchPopularHotelsFilter = function (item) {
        if (typeof $scope.filteredCountriesArr !== 'undefined' && $scope.filteredCountriesArr.length == 1 && $scope.filteredRegionsArr.length != 1) {
            if (item.countryId == $scope.filteredCountriesArr[0].id) {
                return true;
            }
        } else if (typeof $scope.filteredRegionsArr !== 'undefined' && $scope.filteredRegionsArr.length == 1 && $scope.filteredCountriesArr.length == 1) {
            if (item.parent == $scope.selectedRegionId || (item.parent2 == $scope.selectedRegionId && item.parent2 != null)) {
                return true;
            }
        } else {
            return (
            (item.value && angular.lowercase(item.value).indexOf(angular.lowercase($scope.autocompleteWhere.value) || '') !== -1) ||
            (item.value_en && angular.lowercase(item.value_en).indexOf(angular.lowercase($scope.autocompleteWhere.value) || '') !== -1) ||
            (item.value_wr && angular.lowercase(item.value_wr).indexOf(angular.lowercase($scope.autocompleteWhere.value) || '') !== -1)
            );
        }
    };

    $scope.searchFilter = function (item) {
        return (
        (item.value && angular.lowercase(item.value).indexOf(angular.lowercase($scope.autocompleteWhere.value) || '') !== -1) ||
        (item.value_en && angular.lowercase(item.value_en).indexOf(angular.lowercase($scope.autocompleteWhere.value) || '') !== -1) ||
        (item.value_wr && angular.lowercase(item.value_wr).indexOf(angular.lowercase($scope.autocompleteWhere.value) || '') !== -1)
        );
    };

});

IntouristControllers.controller('AutocompleteCityCtrl', function ($scope, $http, $element, $timeout) {
    $scope.inFocus = false;

    $scope.autocompleteToggle = function ($event) {
        if ($event.type == 'focus') {
            $timeout(function () {
                $scope.inFocus = true;
            }, 100);
            $scope.autocompleteVisibility = true;
        }

        if ($event.type == 'blur') {
            $scope.inFocus = false;
            $scope.autocompleteVisibility = false;
        }

        if ($event.type == 'click' && $scope.inFocus) {
            if ($scope.autocompleteCity.value == '') {
                $scope.autocompleteVisibility = !$scope.autocompleteVisibility;
            }
        }

        if ($event.type == 'keyup') {
            $scope.inFocus = false;
            $scope.autocompleteVisibility = true;

            if (typeof $scope.filteredCities !== 'undefined' && $scope.filteredCities.length == 1) {
                $scope.setInputCity($scope.filteredCities[0].id);
            }
        }
    };

    $scope.autocompleteSetValue = function (value) {
        $scope.setAutocompleteCity(value);
    };

    $scope.autocompleteClear = function () {
        $scope.setInputCity('');
        $scope.setAutocompleteCity('');
    };

    // Inputs

    $scope.selectForInputRegion = function (obj) {
        $scope.setInputCity(obj.id);
    };

    $scope.searchFilter = function (item) {
        return (
        (item.value && angular.lowercase(item.value).indexOf(angular.lowercase($scope.autocompleteCity.value) || '') !== -1) ||
        (item.value_en && angular.lowercase(item.value_en).indexOf(angular.lowercase($scope.autocompleteCity.value) || '') !== -1) ||
        (item.value_wr && angular.lowercase(item.value_wr).indexOf(angular.lowercase($scope.autocompleteCity.value) || '') !== -1)
        );
    };

});

IntouristControllers.controller('AutocompleteHotelCtrl', function ($scope, $http, $element, $timeout) {
    $scope.inFocus = false;

    var getHotelsList = function () {
        if($scope.autocompleteHotel.value){
            jQuery.ajax({
                method: 'POST',
                url: '/local/include/ajax/get-hotels-list.php',
                dataType: 'json',
                data: {
                    NAME: $scope.autocompleteHotel.value
                },
            }).done(function (data) {
                $scope.$apply(function () {
                    if (data.length) {
                        $scope.setHotel(jQuery.parseJSON(data));
                    } else {
                        $scope.setHotel([]);
                    }
                });
                /*
                console.log('ajax request for hotels done');
                if (data.length) {
                    console.log(jQuery.parseJSON(data));
                };
                */
            });
        }else{
            $scope.setHotel([]);
        }
    };

    var delayed = _.throttle(getHotelsList, 500);

    $scope.$watch('autocompleteHotel.value', function() {
      delayed();
    });

    $scope.autocompleteToggle = function ($event) {
        if ($event.type == 'focus') {
            $timeout(function () {
                $scope.inFocus = true;
            }, 100);
            $scope.autocompleteVisibility = true;
        }

        if ($event.type == 'blur') {
            $scope.inFocus = false;
            $scope.autocompleteVisibility = false;
        }

        if ($event.type == 'click' && $scope.inFocus) {
            if ($scope.autocompleteHotel.value == '') {
                $scope.autocompleteVisibility = !$scope.autocompleteVisibility;
            }
        }

        if ($event.type == 'keyup') {
            $scope.inFocus = false;
            $scope.autocompleteVisibility = true;

            if (typeof $scope.filteredHotels !== 'undefined' && $scope.filteredHotels !== null && $scope.filteredHotels.length == 1) {
                $scope.setInputCountry($scope.filteredHotels[0].countryId);
                $scope.setInputRegion($scope.filteredHotels[0].parent);
                $scope.setInputHotel($scope.filteredHotels[0].id);
            }
        }
    };


    $scope.autocompleteSetValue = function (value) {
        $scope.setAutocompleteHotel(value);
    };

    $scope.autocompleteClear = function () {
        $scope.setInputHotel('');
        $scope.setInputRegion('');
        $scope.setInputCountry('');
        $scope.setAutocompleteHotel('');
        $scope.setAutocompleteWhere('');
    };

    // Inputs

    $scope.autocompleteSelectCountry = function (parent) {
        var filteredCountry = $scope.country.filter(function (item) {
            return item.id === parent;
        });

        return filteredCountry[0].value;
    };

    $scope.selectForInputHotel = function (obj) {
        var objectRegion = $scope.region.filter(function (item) {
            return item.id === obj.parent;
        });

        $scope.setInputCountry(obj.countryId);
        $scope.setInputRegion(obj.parent);
        $scope.setInputHotel(obj.id);
        $scope.setAutocompleteWhere(objectRegion[0].value);
    };

    $scope.countryRegionFilter = function (item) {
        if ($scope.inputCountry != '' && $scope.inputRegion == '') {

            var countryId = ($scope.inputCountry).match(/[0-9]+/gim);

            if (item.countryId == countryId[0]) {
                return true;
            }

        } else if ($scope.inputRegion != '') {

            var regionId = ($scope.inputRegion).match(/[0-9]+/gim);

            if (item.parent == regionId[0] || (item.parent2 == regionId[0] && item.parent2 != null)) {
                return true;
            }

        } else {
            return true;
        }
    };

    $scope.searchFilter = function (item) {
        return (
            (item.value && angular.lowercase(item.value).indexOf(angular.lowercase($scope.autocompleteHotel.value) || '') !== -1)
        );
    };

});
