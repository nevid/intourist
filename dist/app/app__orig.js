/* INTOURIST AUTOCOMPLETE
-------------------------------------------------- */

var IntouristApp = angular.module('IntouristApp', ['IntouristControllers']);
var IntouristControllers = angular.module('IntouristControllers', []);

IntouristControllers.controller('IntouristMainCtrl', function ($scope, $http, $element) {
  /*
  $http.get('path/to/country.json').success(function(data) {
    $scope.country = data;
  });

  $http.get('path/to/region.json').success(function(data) {
    $scope.region = data;
  });

  $http.get('path/to/hotel.json').success(function(data) {
    $scope.hotel = data;
  });

  $http.get('path/to/city.json').success(function(data) {
    $scope.city = data;
  });
  */

  $scope.inputCountry = '';
  $scope.inputRegion = '';
  $scope.inputHotel = '';
  $scope.inputCity = '';

  $scope.autocompleteWhere = {
    value: ''
  };

  $scope.autocompleteCity = {
    value: ''
  };

  $scope.autocompleteHotel = {
    value: ''
  };

  $scope.country = [
    {
      id: 777,
      title: "Абхазия",
      value: "Абхазия",
      value_en: "Abhazia",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 1,
      title: "Австрия",
      value: "Австрия",
      value_en: "Avstria",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 2,
      title: "Андорра",
      value: "Андорра",
      value_en: "Andorra",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 3,
      title: "Армения",
      value: "Армения",
      value_en: "Armenia",
      disabled: 1,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 4,
      title: "Беларусь",
      value: "Беларусь",
      value_en: "Belarus",
      disabled: 1,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 5,
      title: "Болгария",
      value: "Болгария",
      value_en: "Bolgaria",
      disabled: 0,
      popular: 1,
      popular_weight: 500
    },
    {
      id: 6,
      title: "Вьетнам",
      value: "Вьетнам",
      value_en: "Vietnam",
      disabled: 0,
      popular: 1,
      popular_weight: 300
    },
    {
      id: 7,
      title: "Греция",
      value: "Греция",
      value_en: "Grecia",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 8,
      title: "Грузия",
      value: "Грузия",
      value_en: "Gruzia",
      disabled: 1,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 9,
      title: "Доминикана",
      value: "Доминикана",
      value_en: "Dominikana",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 10,
      title: "Израиль",
      value: "Израиль",
      value_en: "Izrail",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 11,
      title: "Индия",
      value: "Индия",
      value_en: "India",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 12,
      title: "Индонезия",
      value: "Индонезия",
      value_en: "Indonezia",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 13,
      title: "Иордания",
      value: "Иордания",
      value_en: "Iordania",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 14,
      title: "Испания",
      value: "Испания",
      value_en: "Ispania",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 15,
      title: "Италия",
      value: "Италия",
      value_en: "Italia",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 16,
      title: "Кипр",
      value: "Кипр",
      value_en: "Kipr",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 17,
      title: "Китай",
      value: "Китай",
      value_en: "Kitai",
      disabled: 1,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 18,
      title: "Куба",
      value: "Куба",
      value_en: "Kuba",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 19,
      title: "Маврикий",
      value: "Маврикий",
      value_en: "Mavrikiy",
      disabled: 1,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 20,
      title: "Мальдивы",
      value: "Мальдивы",
      value_en: "Maldivi",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 21,
      title: "Марокко",
      value: "Марокко",
      value_en: "Marokko",
      disabled: 1,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 22,
      title: "Мексика",
      value: "Мексика",
      value_en: "Meksika",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 23,
      title: "ОАЭ",
      value: "ОАЭ",
      value_en: "OAE",
      disabled: 0,
      popular: 1,
      popular_weight: 400
    },
    {
      id: 24,
      title: "Россия",
      value: "Россия",
      value_en: "Rossia",
      disabled: 0,
      popular: 1,
      popular_weight: 100
    },
    {
      id: 25,
      title: "Сейшеллы",
      value: "Сейшеллы",
      value_en: "Seyshelly",
      disabled: 1,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 26,
      title: "Таиланд",
      value: "Таиланд",
      value_en: "Tailand",
      disabled: 0,
      popular: 1,
      popular_weight: 200
    },
    {
      id: 27,
      title: "Тунис",
      value: "Тунис",
      value_en: "Tunis",
      disabled: 1,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 28,
      title: "Турция",
      value: "Турция",
      value_en: "Turcia",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 29,
      title: "Франция",
      value: "Франция",
      value_en: "Francia",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 30,
      title: "Хорватия",
      value: "Хорватия",
      value_en: "Horvatia",
      disabled: 1,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 31,
      title: "Черногория",
      value: "Черногория",
      value_en: "Chernogoria",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 32,
      title: "Чехия",
      value: "Чехия",
      value_en: "Chehia",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 33,
      title: "Шри-Ланка",
      value: "Шри-Ланка",
      value_en: "Shri-Lanka",
      disabled: 0,
      popular: 0,
      popular_weight: 0
    },
    {
      id: 34,
      title: "Ямайка",
      value: "Ямайка",
      value_en: "Yamaika",
      disabled: 1,
      popular: 0,
      popular_weight: 0
    }
  ];

  /*!
   * id: id региона
   * parent: id страны к коророй относится регион
   * title: наименование регоина
   * disabled: вкл/выкл регоин
   */

  $scope.region = [
    {
      id: 888,
      parent: 777,
      title: 'Абхазия регион 1',
      value: 'Абхазия регион 1',
      value_en: '',
      disabled: 0
    },
    {
      id: 1,
      parent: 777,
      title: 'Абхазия регион 2',
      value: 'Абхазия регион 2',
      value_en: '',
      disabled: 0
    },
    {
      id: 2,
      parent: 777,
      title: 'Абхазия регион 3',
      value: 'Абхазия регион 3',
      value_en: '',
      disabled: 0
    },

    {
      id: 3,
      parent: 1,
      title: 'Австрия регион 1',
      value: 'Австрия регион 1',
      value_en: '',
      disabled: 0
    },
    {
      id: 4,
      parent: 1,
      title: 'Австрия регион 2',
      value: 'Австрия регион 2',
      value_en: '',
      disabled: 0
    },
    {
      id: 5,
      parent: 1,
      title: 'Австрия регион 3',
      value: 'Австрия регион 3',
      value_en: '',
      disabled: 0
    }
  ];

  /*!
   * id: id отеля
   * parent: id региона к коророму относится отель
   * title: наименование отеля
   * disabled: вкл/выкл отель
   */

  $scope.hotel = [
    {
      id: 0,
      parent: 888,
      title: 'отель Абхазия регион 1 отель 1',
      value: 'отель Абхазия регион 1 отель 1',
      value_en: '',
      disabled: 0
    },
    {
      id: 1,
      parent: 888,
      title: 'отель Абхазия регион 1 отель 2',
      value: 'отель Абхазия регион 1 отель 2',
      value_en: '',
      disabled: 0
    },
    {
      id: 2,
      parent: 888,
      title: 'отель Абхазия регион 1 отель 3',
      value: 'отель Абхазия регион 1 отель 3',
      value_en: '',
      disabled: 0
    },
    {
      id: 3,
      parent: 1,
      title: 'Абхазия регион 2 отель 1',
      value: 'Абхазия регион 2 отель 1',
      value_en: '',
      disabled: 0
    },
    {
      id: 4,
      parent: 1,
      title: 'Абхазия регион 2 отель 2',
      value: 'Абхазия регион 2 отель 2',
      value_en: '',
      disabled: 0
    },
    {
      id: 5,
      parent: 1,
      title: 'Абхазия регион 2 отель 3',
      value: 'Абхазия регион 2 отель 3',
      value_en: '',
      disabled: 0
    },
    {
      id: 6,
      parent: 2,
      title: 'Абхазия регион 3 отель 1',
      value: 'Абхазия регион 3 отель 1',
      value_en: '',
      disabled: 0
    },
    {
      id: 7,
      parent: 2,
      title: 'Абхазия регион 3 отель 2',
      value: 'Абхазия регион 3 отель 2',
      value_en: '',
      disabled: 0
    },
    {
      id: 8,
      parent: 2,
      title: 'Абхазия регион 3 отель 3',
      value: 'Абхазия регион 3 отель 3',
      value_en: '',
      disabled: 0
    },

    {
      id: 9,
      parent: 3,
      title: 'Австрия регион 1 отель 1',
      value: 'Австрия регион 1 отель 1',
      value_en: '',
      disabled: 0
    },
    {
      id: 10,
      parent: 3,
      title: 'Австрия регион 1 отель 2',
      value: 'Австрия регион 1 отель 2',
      value_en: '',
      disabled: 0
    },
    {
      id: 11,
      parent: 3,
      title: 'Австрия регион 1 отель 3',
      value: 'Австрия регион 1 отель 3',
      value_en: '',
      disabled: 0
    },
    {
      id: 12,
      parent: 4,
      title: 'Австрия регион 2 отель 1',
      value: 'Австрия регион 2 отель 1',
      value_en: '',
      disabled: 0
    },
    {
      id: 13,
      parent: 4,
      title: 'Австрия регион 2 отель 2',
      value: 'Австрия регион 2 отель 2',
      value_en: '',
      disabled: 0
    },
    {
      id: 14,
      parent: 4,
      title: 'Австрия регион 2 отель 3',
      value: 'Австрия регион 3 отель 3',
      value_en: '',
      disabled: 0
    },
    {
      id: 15,
      parent: 5,
      title: 'Австрия регион 3 отель 1',
      value: 'Австрия регион 3 отель 1',
      value_en: '',
      disabled: 0
    },
    {
      id: 16,
      parent: 5,
      title: 'Австрия регион 3 отель 2',
      value: 'Австрия регион 3 отель 2',
      value_en: '',
      disabled: 0
    },
    {
      id: 17,
      parent: 5,
      title: 'Австрия регион 3 отель 3',
      value: 'Австрия регион 3 отель 3',
      value_en: '',
      disabled: 0
    }
  ];

  /*!
   * id: id города
   * title: наименование регоина
   * disabled: вкл/выкл регоин
   */

  $scope.city = [
    {
      id: 0,
      title: 'Самара',
      value: 'Самара',
      value_en: 'Samara',
      disabled: 0
    },
    {
      id: 1,
      title: 'Москва',
      value: 'Москва',
      value_en: 'Moskva',
      disabled: 0
    },
    {
      id: 2,
      title: 'Санкт-Петербург',
      value: 'Санкт-Петербург',
      value_en: 'Sankt-Peterburg',
      disabled: 0
    },

    {
      id: 3,
      title: 'Тольятти',
      value: 'Тольятти',
      value_en: 'Toliatti',
      disabled: 0
    },
    {
      id: 4,
      title: 'Екатеринбург',
      value: 'Екатеринбург',
      value_en: 'Eketerinburg',
      disabled: 0
    },
    {
      id: 5,
      title: 'Сочи',
      value: 'Сочи',
      value_en: 'Sochi',
      disabled: 0
    }
  ];

  $scope.setCountry = function (data) {
    $scope.country = data;
  };

  $scope.setRegion = function (data) {
    $scope.region = data;
  };

  $scope.setHotel = function (data) {
    $scope.hotel = data;
  };

  $scope.setCity = function (data) {
    $scope.city = data;
  };

  $scope.setInputCountry = function (data) {
    $scope.inputCountry = data;
  };

  $scope.setInputRegion = function (data) {
    $scope.inputRegion = data;
  };

  $scope.setInputHotel = function (data) {
    $scope.inputHotel = data;
  };

  $scope.setInputCity = function (data) {
    $scope.inputCity = data;
  };

  $scope.setAutocompleteWhere = function (data) {
    $scope.autocompleteWhere.value = data;
  };

  $scope.setAutocompleteCity = function (data) {
    $scope.autocompleteCity.value = data;
  };

  $scope.setAutocompleteHotel = function (data) {
    $scope.autocompleteHotel.value = data;
  };

  $scope.countUniqueLength = function (arr) {
    if (arr.length == 0) return 0;

    var unique_cnt = 1;
    var unique_flag = true;

    for (var i = 1; i < arr.length; i++) {
      unique_flag = true;

      for (var j = i - 1; j >= 0; j--) {
        if (arr[j] == arr[i]) unique_flag = false;
        if (unique_flag) unique_cnt++;
      };
    };

    return unique_cnt;
  };

  $scope.autocompleteSelectCountryByRegion = function (parent) {
    var filteredCountry = $scope.country.filter(function (item) {
      return item.id === parent;
    });

    return filteredCountry[0].title;
  };

  $scope.autocompleteSelectCountryByHotel = function (parent) {
    var filteredRegion = $scope.region.filter(function (item) {
      return item.id === parent;
    });

    var filteredCountry = $scope.country.filter(function (item) {
      return item.id === filteredRegion[0].parent;
    });

    return filteredCountry[0].title;
  };

});

IntouristControllers.controller('AutocompleteWhereCtrl', function ($scope, $http, $element) {
  $scope.selectedCountry = '';
  $scope.selectedRegion = '';

  /*  $scope.inputCountry = '';
    $scope.inputRegion = '';
    $scope.inputHotel = '';*/

  $scope.uniqueHotels = [];
  $scope.uniqueRegions = [];

  $scope.autocompleteToggle = function (toggle) {
    $scope.autocompleteVisibility = toggle;
  };

  $scope.selectCountryAndRegion = function (value) {
    $scope.selectedCountry = '';
    $scope.selectedRegion = '';
    $scope.uniqueHotels.length = 0;
    $scope.uniqueRegions.length = 0;

    if (typeof $scope.filteredHotels != 'undefined') {
      if ($scope.filteredHotels.length) {
        for (var i = 0; i < $scope.filteredHotels.length; i++) {
          $scope.uniqueHotels.push($scope.filteredHotels[i].parent);
        };

        if ($scope.countUniqueLength($scope.uniqueHotels) == 1) {
          var filteredRegion = $scope.region.filter(function (item) {
            return item.id === $scope.filteredHotels[0].parent;
          });

          var filteredCountry = $scope.country.filter(function (item) {
            return item.id === filteredRegion[0].parent;
          });

          $scope.selectedCountry = filteredCountry[0].title;
          $scope.selectedRegion = filteredRegion[0].title;
          $scope.selectedHotel = $scope.filteredHotels[0].title;
        };
      };
    };

    if (typeof $scope.filteredRegions != 'undefined') {
      if ($scope.filteredRegions.length && $scope.filteredRegions.length == 1) {
        var filteredCountry = $scope.country.filter(function (item) {
          return item.id === $scope.filteredRegions[0].parent;
        });

        $scope.selectedCountry = filteredCountry[0].title;
      };
    };
  };

  $scope.autocompleteSetValue = function (value) {
    /*$scope.autocompleteWhere = value;*/
    $scope.setAutocompleteWhere(value);
    $scope.autocompleteVisibility = true;

    $scope.$watch('autocompleteWhere', function (newValue, oldValue) {
      $scope.selectCountryAndRegion(newValue);
    });
  };

  // autocomplete select values

  /*  $scope.autocompleteSelectCountryByRegion = function(parent) {
      var filteredCountry = $scope.country.filter(function(item) {
        return item.id === parent;
      });

      return filteredCountry[0].title;
    };*/

  /*  $scope.autocompleteSelectCountryByHotel = function(parent) {
      var filteredRegion = $scope.region.filter(function(item) {
        return item.id === parent;
      });

      var filteredCountry = $scope.country.filter(function(item) {
        return item.id === filteredRegion[0].parent;
      });

      return filteredCountry[0].title;
    };*/

  // Inputs

  $scope.selectForInputCountry = function (obj) {
    /*    $scope.inputCountry = '{countryId: ' + obj.id + '}';
        $scope.inputRegion = '';
        $scope.inputHotel = '';*/
    $scope.setInputCountry('{countryId: ' + obj.id + '}');
    $scope.setInputRegion('');
    $scope.setInputHotel('');
  };

  $scope.selectForInputRegion = function (obj) {
    var objectCountry = $scope.country.filter(function (item) {
      return item.id === obj.parent;
    });

    /*    $scope.inputCountry = '{countryId: ' + objectCountry[0].id + '}';
        $scope.inputRegion = '{regionId: ' + obj.id + '}';
        $scope.inputHotel = '';*/
    $scope.setInputCountry('{countryId: ' + objectCountry[0].id + '}');
    $scope.setInputRegion('{regionId: ' + obj.id + '}');
    $scope.setInputHotel('');
  };

  $scope.selectForInputHotel = function (obj) {
    var objectRegion = $scope.region.filter(function (item) {
      return item.id === obj.parent;
    });

    var objectCountry = $scope.country.filter(function (item) {
      return item.id === objectRegion[0].parent;
    });

    /*    $scope.inputCountry = '{countryId: ' + objectCountry[0].id + '}';
        $scope.inputRegion = '{regionId: ' + objectRegion[0].id + '}';
        $scope.inputHotel = '{hotelId: ' + obj.id + '}';*/
    $scope.setInputCountry('{countryId: ' + objectCountry[0].id + '}');
    $scope.setInputRegion('{regionId: ' + objectRegion[0].id + '}');
    $scope.setInputHotel('{hotelId: ' + obj.id + '}');
  };

  $scope.autocompleteClear = function () {
    /*    $scope.inputCountry = '';
        $scope.inputRegion = '';
        $scope.inputHotel = '';*/
    $scope.setInputCountry('');
    $scope.setInputRegion('');
    $scope.setInputHotel('');
    /*$scope.autocompleteWhere = '';*/
    $scope.setAutocompleteWhere('');
    $scope.setAutocompleteHotel('');
  };

  $scope.searchFilter = function (item) {
    return (
      angular.lowercase(item.value).indexOf(angular.lowercase($scope.autocompleteWhere.value) || '') !== -1 ||
      angular.lowercase(item.value_en).indexOf(angular.lowercase($scope.autocompleteWhere.value) || '') !== -1
    );
  };

});

IntouristControllers.controller('AutocompleteCityCtrl', function ($scope, $http, $element) {
  /*$scope.inputCity = '';*/

  $scope.autocompleteToggle = function (toggle) {
    $scope.autocompleteVisibility = toggle;
  };

  $scope.autocompleteSetValue = function (value) {
    /*$scope.autocompleteCity = value;*/
    $scope.setAutocompleteCity(value);
  };

  $scope.autocompleteClear = function () {
    /*$scope.inputCity = '';*/
    $scope.setInputCity('');
    /*$scope.autocompleteCity = '';*/
    $scope.setAutocompleteCity('');
  };

  // Inputs

  $scope.selectForInputRegion = function (obj) {
    /*$scope.inputCity = '{cityId: ' + obj.id + '}';*/
    $scope.setInputCity('{cityId: ' + obj.id + '}');
  };

  $scope.searchFilter = function (item) {
    return (
      angular.lowercase(item.value).indexOf(angular.lowercase($scope.autocompleteCity.value) || '') !== -1 ||
      angular.lowercase(item.value_en).indexOf(angular.lowercase($scope.autocompleteCity.value) || '') !== -1
    );
  };

});

IntouristControllers.controller('AutocompleteHotelCtrl', function ($scope, $http, $element) {
  /*$scope.inputHotel = '';*/

  $scope.autocompleteToggle = function (toggle) {
    $scope.autocompleteVisibility = toggle;
  };

  $scope.autocompleteSetValue = function (value) {
    /*$scope.autocompleteHotel = value;*/
    $scope.setAutocompleteHotel(value);
  };

  $scope.autocompleteClear = function () {
    /*$scope.inputHotel = '';*/
    $scope.setInputHotel('');
    $scope.setInputRegion('');
    $scope.setInputCountry('');
    /*$scope.autocompleteHotel = '';*/
    $scope.setAutocompleteHotel('');
    $scope.setAutocompleteWhere('');
  };

  // Inputs

  /*  $scope.selectForInputHotel = function(obj) {
      $scope.inputHotel = '{hotelId: '+ obj.id +'}';
    };*/

  $scope.selectForInputHotel = function (obj) {
    var objectRegion = $scope.region.filter(function (item) {
      return item.id === obj.parent;
    });

    var objectCountry = $scope.country.filter(function (item) {
      return item.id === objectRegion[0].parent;
    });

    /*    $scope.inputCountry = '{countryId: ' + objectCountry[0].id + '}';
        $scope.inputRegion = '{regionId: ' + objectRegion[0].id + '}';
        $scope.inputHotel = '{hotelId: ' + obj.id + '}';*/
    $scope.setInputCountry('{countryId: ' + objectCountry[0].id + '}');
    $scope.setInputRegion('{regionId: ' + objectRegion[0].id + '}');
    $scope.setInputHotel('{hotelId: ' + obj.id + '}');
    $scope.setAutocompleteWhere(objectRegion[0].title);
  };

  $scope.searchFilter = function (item) {
    return (
      angular.lowercase(item.value).indexOf(angular.lowercase($scope.autocompleteHotel.value) || '') !== -1 ||
      angular.lowercase(item.value_en).indexOf(angular.lowercase($scope.autocompleteHotel.value) || '') !== -1
    );
  };

});