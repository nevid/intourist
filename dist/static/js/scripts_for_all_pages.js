//------------------------ФУНКЦИОНАЛ ЧЕКБОКСА "ВСЕ" ВО ВСЕХ ЭЛЕМЕНТАХ ФОРМЫ----------------
//ВНИМАНИЕ! я поменял немного скрипт, так что нужно его подменить уже на прикрученном
$(document).ready(function() {
    //Выбрать все чекбоксы
    $('li.select-all').click(function() {
        if ($(this).hasClass('-active')) {
            $(this).parent().removeClass('selected-all');
        } else {
            $(this).parent().addClass('selected-all');
        }
    });
    $('.quick-dropdown__list li').click(function() {
        if (!$(this).hasClass('select-all')) {
            $(this).parent().removeClass('selected-all');
            $(this).parent().find('li.select-all').removeClass('-active');
        }
    });
});


//-----------------ФУНКЦИОНАЛ СИНЕГО МЕНЮ-----------------------------------------------------------
$(document).ready(function() {
    $('#dl-menu').dlmenu();
    if ($(window).width() < 1280) {
        dl_menu_resize();
        $(window).resize(function() {
            dl_menu_resize();
        });

        function dl_menu_resize() {
            var wH = window.innerHeight;
            var elT = $('ul.dl-menu').offset().top - $(window).scrollTop();
            var elBlT = $('.dl-menu-block').offset().top - $(window).scrollTop();
            var elH = wH - elT;
            var elBlH = wH - elBlT;

            $('ul.dl-menu').height(elH);
            $('.dl-menu-block').height(elBlH);
        }
    }
	
	//подсветка активных пунктов при наведении
    $('#dl-menu > .dl-menu').on('mouseenter mouseleave', '> .list__item > .list__link', function(e) {
        if ($(window).width() >= 1280) {
            if (e.type == 'mouseenter') {
                $(this).parent().siblings('.list__item').addClass('-not-active');
            };

            if (e.type == 'mouseleave') {
                $(this).parent().siblings('.list__item').removeClass('-not-active');
            };
        };
    });
	
    //Вёрстка плейсхолдеров
	$('input[placeholder]').inputHints();
});	


//-----------------МОБИЛЬНАЯ ВЕРСИЯ. ОТКРЫТИЕ БЕЛОЙ ФОРМЫ ПРИ КЛИКЕ НА НОВЫЙ ПОИСК-----------------------------------------------------------
$(document).ready(function() {
    //Открытие белой формы в мобильной версии при клике на кнопку поиск тура
    if ($(window).width() <= 768) {
        $('.tour-filter__toggle__mobile').click(function() {
            $(this).toggleClass('open');
            $('.left-sidebar').toggleClass('active');
        });
    }
});


//-----------------МОБИЛЬНАЯ ВЕРСИЯ. ФУНКЦИОНАЛ ПЕРЕКЛЮЧЕНИЯ ВСПЛЫВАЮЩИХ ОКОН ПОЛЯ "КУДА"-----------------------------------------------------------
// Я так понимаю теперь навигация поменялась и старую нужно вообще убрать. 
//поэтому не уверен,что этот скрипт целесообразен. посмотри, как у тебя работает навигация на десктопе.
$(document).ready(function() {
    //Всплывающие окна в мобильной версии при клике на поле фильтра Куда
    if ($(window).width() < 768) {
        $('.all-countries li').click(function() {
            $('div.wrapper-data.all-countries').addClass('hiden');
            $('div.wrapper-data.all-countries').removeClass('active');
            $('div.filter-popup').removeClass('all-countries');
            $('div.filter-popup').addClass('resorts');
            $('div.wrapper-data.resorts').removeClass('hiden');
            $('div.wrapper-data.resorts').addClass('active');
            $('.select-city').show();
        });

        $('.filter-popup__buttons .next ').click(function() {
            $('div.wrapper-data.resorts').removeClass('active');
            $('div.wrapper-data.resorts').addClass('hiden');
            $('div.filter-popup').removeClass('resorts');
            $('div.filter-popup').addClass('hotels');
            $('div.wrapper-data.hotels').removeClass('hiden');
            $('div.wrapper-data.hotels').addClass('active');
        });

        $('.filter-popup__buttons .cancel').click(function() {
            if ($('div.wrapper-data.resorts').hasClass('active')) {
                $('div.wrapper-data.resorts').removeClass('active');
            } else if (!$('div.wrapper-data.resorts').hasClass('hiden')) {
                $('div.wrapper-data.resorts').addClass('hiden');
            }

            if ($('div.wrapper-data.hotels').hasClass('active')) {
                $('div.wrapper-data.hotels').removeClass('active');
            } else if (!$('div.wrapper-data.hotels').hasClass('hiden')) {
                $('div.wrapper-data.hotels').addClass('hiden');
            }

            if ($('div.wrapper-data.all-countries').hasClass('active')) {
                $('div.wrapper-data.all-countries').removeClass('active');
            } else if (!$('div.wrapper-data.all-countries').hasClass('hiden')) {
                $('div.wrapper-data.all-countries').addClass('hiden');
            }

            $('div.filter-popup').removeClass('resorts');
            $('div.filter-popup').removeClass('hotels');
            $('div.filter-popup').removeClass('all-countries');
            var to_choosen = $('#to-autocomplete');
            var to_data_select = to_choosen.find('.autocomplete');
            //to_choosen.removeClass('autocomplete-open');
            to_data_select.addClass('autocomplete-open');

        });
    }
});