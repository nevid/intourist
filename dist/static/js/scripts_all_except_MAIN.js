jQuery(document).ready(function($) {

	//-------------------------------МОБИЛЬНАЯ. ОТКРЫТИЕ БЕЛОГО ФИЛЬТРА-----------------------
	$('.tour-filter__toggle__mobile').click(function() {
		$('header.header').toggleClass('open-filter');
		$(this).toggleClass('open');
		$('.tour-filter.main-filter').toggleClass('active');
		$('body').toggleClass('opened-filter');
	});
	
	
	//-------------------------------ДЕСКТОП. ПОКАЗ/СКРЫТИЕ ФИЛЬТРОВ ПРИ КЛИКЕ ПО КНОПКЕ "ПОИСК ТУРА"-----------------------
	$('.tour-filter__toggle').click(function() {
		$('header.header').toggleClass('open-filter');
		$(this).toggleClass('open');
		$('.tour-filter.main-filter').toggleClass('active');
	});
	
});