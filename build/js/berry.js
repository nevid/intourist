/*!
 * @name berry
 * @version 0.03
 * @author pro-verstka@yandex.ru
 * @date 24/10/2013
 * @update 10/02/2016
 */

/*!
 * @template
 *
  <label for="check0">Checkbox 0</label>
  <input type="checkbox" name="check0" id="check0" class="berry" checked>
 */

(function($) {
  $.fn.berry = function(options) {
    var
      opts = $.extend({}, $.fn.berry.defaults, options);

    return this.each(function() {
      $this = $(this);

      var
        o = $.meta ? $.extend({}, opts, $this.data()) : opts;

      $berry = {
        prepare: function() {
          $this.each(function() {
            var $input = $(this);
            var $label = $('label[for="'+ $input.attr('id') +'"]');
            var typeClass = '';
            var emptyClass = '';
            var statusClass = '-berry-status-enabled';
            var directionClass = '-berry-direction-'+ o.direction;

            if ( $input.is(':checkbox') ) typeClass = '-checkbox';
            if ( $input.is(':radio') ) typeClass = '-radio';
            if ( $input.is(':disabled') ) statusClass = '-berry-status-disabled';
            if ( $label.is(':empty') ) emptyClass = '-berry-empty';

            var template = $('<div class="berry-input -berry-type'+ typeClass +' '+ directionClass +' '+ statusClass +' '+ emptyClass +' '+ o.customClass +' "><b></b></div>');

            $input.after(template).appendTo(template);
            $label.appendTo(template);
          });

          this.toggle();
        },

        toggle: function() {
          $('.berry-input').each(function() {
            var $self = $(this);

            if ( $self.find('input').prop('checked') ) {
              $self.addClass('-berry-checked');
            } else {
              $self.removeClass('-berry-checked');
            };

            if ( $self.find('input').prop('disabled') ) {
              $self.removeClass('-berry-status-enabled').addClass('-berry-status-disabled');
            } else {
              $self.removeClass('-berry-status-disabled').addClass('-berry-status-enabled');
            };
          });
        }
      };

      $this.on('change', function() {
        $berry.toggle();
      });

      $berry.prepare();

    });
  };

  //defaults
  $.fn.berry.defaults = {
    customClass: '-berry-custom',
    direction: 'ltr' //ltr, rtl
  };

})(jQuery);