jQuery(document).ready(function($){
  $('.hamburger').click(function(){
    if(!$(this).hasClass('is-active')){
      $(this).addClass('is-active');
      $('.header').slideDown();
    }
    else {
      $(this).removeClass('is-active');
      $('.header').slideUp();      
    }
  });
});