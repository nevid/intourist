var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minifyCSS = require('gulp-cssnano');
var RevAll = require('gulp-rev-all');
var archiver = require('gulp-archiver');

var less = require('gulp-less');
var ntk = require('gulp-less');
var lessPluginAutoPrefix = require('less-plugin-autoprefix');
var autoprefixPlugin = new lessPluginAutoPrefix({
  browsers: ['last 3 version', 'ie 9']
});

var browserSync = require('browser-sync');
var reload = browserSync.reload;

gulp.task('browser-sync', function () {
  browserSync({
    server: {
      baseDir: './dist/',
      directory: true
    },
    notify: true
  });
});

gulp.task('less', function () {
  gulp.src('build/css/general.less')
    .pipe(less({
      plugins: [autoprefixPlugin],
      compress: true
    }))
    .pipe(concat('general.css'))
    .pipe(minifyCSS({
      zindex: false
    }))
    .pipe(gulp.dest('dist/static/css/'))
    .pipe(reload({
      stream: true
    }));
});

gulp.task('ntk', function () {
  gulp.src('build/css/ntk.less')
    .pipe(less({
      plugins: [autoprefixPlugin],
      compress: true
    }))
    .pipe(concat('ntk.css'))
    .pipe(minifyCSS({
      zindex: false
    }))
    .pipe(gulp.dest('dist/static/css/'))
    .pipe(reload({
      stream: true
    }));
});

gulp.task('minify', function () {
  gulp.src([
      'build/js/fancybox/jquery.fancybox.js',
      //'build/js/fancybox/jquery.fancybox-buttons.js',
      //'build/js/fancybox/jquery.fancybox-media.js',
      //'build/js/fancybox/jquery.fancybox-thumbs.js',
      'build/js/plugin.js',
      'build/js/datepick.js',
      'build/js/slick.js',
      'build/js/berry.js',
      'build/js/touch-punch.js',
      'build/js/translit.js',
      'build/js/video.js',
      'build/js/sticky-kit.js',
      'build/js/mousewheel.js',
      'build/js/jscrollpane.js',
      'build/js/ikselect.js'
      //'build/js/*.js'
     ])
    .pipe(concat('libs.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist/static/js/'))
    .pipe(reload({
      stream: true
    }));
});

gulp.task('static-js', function () {
  gulp.src('dist/static/js/moonkake.js')
    .pipe(reload({
      stream: true
    }));
});

gulp.task('static-html', function () {
  gulp.src('dist/*.html')
    .pipe(reload({
      stream: true
    }));
});

gulp.task('zip', function () {
  var path = require('path');
  var package = path.parse(__dirname);

  var revAll = new RevAll({
    transformFilename: function (file, hash) {
      function addZero(number) {
        return number < 10 ? '0' + number : '' + number;
      };

      var date = new Date();
      var day = addZero(date.getDate());
      var month = addZero(date.getMonth() + 1);
      var year = date.getFullYear();
      var hours = addZero(date.getHours());
      var minutes = addZero(date.getMinutes());

      return package.name + '.' + day + '.' + month + '.' + year + '_' + hours + '.' + minutes + '.zip';
    }
  });

  gulp.src([
      'build/**/*.*',
      'dist/**/*.*',
      'gulpfile.js',
      'package.json'
     ], {
      base: "."
    })
    .pipe(archiver(package.name + '.zip'))
    .pipe(revAll.revision())
    .pipe(gulp.dest('./zip'));
});

gulp.task('default', ['less', 'minify', 'static-js', 'static-html', 'browser-sync'], function () {
  gulp.watch('build/css/**/*.less', ['less']);
  gulp.watch('build/css/**/*.css', ['less']);
  gulp.watch('build/js/**/*.js', ['minify']);
  gulp.watch('dist/static/js/moonkake.js', ['static-js']);
  gulp.watch('dist/*.html', ['static-html']);
});